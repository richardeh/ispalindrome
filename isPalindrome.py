"""
isPalindrome.py
Checks if an input word or string is a palindrome.

Author: Richard Harrington
Created: 9/4/2013
Last update: 9/4/2013
"""

import StringReverser

def main():
    s=input("Please input a word or string to check:\n")

    sCompare=StringReverser.reverser(s)
    sCompare="".join(sCompare)
    
    if s==sCompare:
        print(s," is a palindrome.")
    else:
        print(s," is not a palindrome.")

main()


