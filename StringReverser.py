"""
StringReverser.py
Reads in a string and prints the reverse

Author: Richard Harrington

Created date: 9/3/2013
Last update: 9/3/2013

"""

def reverser(s):
    reverse=[]
    for i in range(0,len(s)):
        reverse.append(s[len(s)-i-1])
    return reverse

#print ("".join(reverser(input("Input a string to reverse:\n"))))
